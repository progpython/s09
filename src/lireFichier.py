# -*- coding: UTF-8 -*-
'''
Created on 2014-03-10
updated on 2022-08-06 -- Python 3.10
@author: Johnny Tsheke
'''
import os # Pour manipuler les chemis des fichiers

def lectureFichier(nomFichier):
    '''cette fonction lit et affiche le contenu du fichier
       dont le nom est passé en argument
    '''
    vFichier=open(nomFichier,"r")
    
    for line in vFichier:
        print(line)
    vFichier.close() #fermeture du fichier

fpath = os.path.dirname(os.path.realpath(__file__)) # chemin dossier du script
fich=os.path.join(fpath,"fichier1.txt") # concatenation chemin et nom de fichier
lectureFichier(fich)