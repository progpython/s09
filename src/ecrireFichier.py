# -*- coding: UTF-8 -*-
'''
Created on 2014-03-10
updated on 2022-08-06 -- Python 3.10
@author: Johnny Tsheke 
'''
import os

def ecritureFichier(nomFichier, liste):
    ''' ecriture des données dans un fichier
    '''
    vFichier=open(nomFichier,"w")
    
    for elem in liste:
        ligne=str(elem)+"\n"
        vFichier.write(ligne)
    print("Fichier "+nomFichier+" écrit avec succès!")
    vFichier.close()#Fermeture du fichier

nombres=[12,5,8,22]
fpath = os.path.dirname(os.path.realpath(__file__))
fich=os.path.join(fpath,"fichier2.txt")
ecritureFichier(fich,nombres)