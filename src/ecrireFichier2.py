# -*- coding: UTF-8 -*-
'''
Created on 2014-03-10
updated on 2022-08-06 -- Python 3.10
@author: Johnny Tsheke 
'''
import os # Pour manipuler chemin fichiers
def ecritureFichier(nomFichier, liste):
    ''' ecriture des données dans un fichier
    '''
    vFichier=open(nomFichier,"w")
    
    for elem in liste:
        print(elem,file=vFichier) # ecriture dans le fichier
    print("Fichier "+nomFichier+" écrit avec succès!")
    vFichier.close()#Fermeture du fichier

nombres=[12,5,8,22]
fpath = os.path.dirname(os.path.realpath(__file__)) # Dossier du script
fich=os.path.join(fpath,"fichier3.txt") #concatenation chemin et fichier
ecritureFichier(fich,nombres)